import React, { Component } from 'react';

import { createStackNavigator } from 'react-navigation';

import SignIn from "./scene/SignIn";
import SignUp from "./scene/SignUp";
import Groups from "./scene/Groups";
import GroupChat from "./scene/Groups/GroupChat";
import JoinGroup from "./scene/Groups/JoinGroup";
import VideoCall from "./scene/VideoCall";
import GroupVideoCall from "./scene/VideoCall/GroupVideoCall";

const Navigators = createStackNavigator(
    {
        SignIn: SignIn,
        SignUp: SignUp,
        Groups: Groups,
        GroupChat: GroupChat,
        JoinGroup: JoinGroup,
        VideoCall: VideoCall,
        GroupVideoCall: GroupVideoCall
    },
    {
        initialRouteName: 'SignIn'
    }
);

class Layout extends Component {

    render() {
        return(
            <Navigators />
        );
    }
}

export default Layout;
