import React, { Component } from 'react';

import { Image, ScrollView } from 'react-native';

import {
    Container,
    Content,
    Text,
    Button,
    View,
    Icon
} from 'native-base';

import _ from 'lodash';

import styles from './styles';

import GroupPhoto0 from '../../assets/images/VideoChat2.jpg';
import GroupPhoto1 from '../../assets/images/group-photo-1.jpg';
import GroupPhoto2 from '../../assets/images/group-photo-2.jpg';
import GroupPhoto3 from '../../assets/images/group-photo-3.jpg';
import GroupPhoto4 from '../../assets/images/123.jpeg';

import videoCallDisconnect from '../../assets/images/video-call-disconnect-button.jpeg';

class GroupVideoCall extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            header: null
        };
    }

    render() {

        return (
            <Container>
                <Content style={styles.videoCallContainer}>
                    <View style={styles.groupPhotos}>
                        <Image source={GroupPhoto0} style={styles.groupPhoto} />
                        <Image source={GroupPhoto1} style={styles.groupPhoto} />
                        <Image source={GroupPhoto2} style={styles.groupPhoto} />
                        <Image source={GroupPhoto3} style={styles.groupPhoto} />
                        <Image source={GroupPhoto4} style={styles.groupPhoto} />
                        <Image source={GroupPhoto0} style={styles.groupPhoto} />
                        <Image source={GroupPhoto1} style={styles.groupPhoto} />
                        <Image source={GroupPhoto2} style={styles.groupPhoto} />
                        <Image source={GroupPhoto3} style={styles.groupPhoto} />
                        <Image source={GroupPhoto4} style={styles.groupPhoto} />
                        <Image source={GroupPhoto0} style={styles.groupPhoto} />
                        <Image source={GroupPhoto1} style={styles.groupPhoto} />
                        <Image source={GroupPhoto4} style={styles.groupPhoto} />
                        <Image source={GroupPhoto4} style={styles.groupPhoto} />
                        <Image source={GroupPhoto4} style={styles.groupPhoto} />
                    </View>

                    <View style={styles.videoTimer}>
                        <Icon style={styles.iconTimer} name="md-stopwatch" /><Text style={{color: '#fff', fontSize: 12, paddingTop: 10}}>00:46:12</Text>
                    </View>
                    
                    <View style={styles.groupVideoFooterButtons}>
                        <View style={styles.roundedBtn}>
                            <Button transparent light>
                                <Icon color='black' style={styles.iconItem} name="ios-volume-low" />
                            </Button>
                        </View>
                        <View style={styles.roundedBtn}>
                            <Button transparent light>
                                <Icon color='black' style={styles.iconItem} name="md-mic" />
                            </Button>
                        </View>
                        <View style={{marginRight: 10}}>
                            <Button transparent light>
                                <Image source={videoCallDisconnect} style={styles.videoCallDisconnect} />
                            </Button>
                        </View>
                        <View style={styles.roundedBtn}>
                            <Button transparent light>
                                <Icon color='black' style={styles.iconItem} name="md-videocam" />
                            </Button>
                        </View>
                        <View style={styles.roundedBtn}>
                            <Button transparent light>
                                <Icon color='black' style={styles.iconItem} name="md-text" />
                            </Button>
                        </View>
                    </View>

                </Content>
            </Container>
        );
    }
}

export default GroupVideoCall;

