import React, { Component } from 'react';

import { Image, ScrollView } from 'react-native';

import {
	Container,
	Content,
	Text,
    Button,
    View,
    Icon
} from 'native-base';

import _ from 'lodash';

import styles from './styles';
import videoCall from '../../assets/images/VideoChat2.jpg';
import videoCallSmallImage from '../../assets/images/video-call-small.png';
import VideoChat from '../../assets/images/VideoChat.jpg';
import videoCallDisconnect from '../../assets/images/video-call-disconnect-button.jpeg';

class VideoCall extends Component {

	static navigationOptions = ({ navigation }) => {
        return {
            header: null
        };
    }

    render() {

        return (
            <Container>
            	<Content style={styles.videoCallContainer}>
                    <Image source={videoCall} style={styles.videoCallImage} />

                    <View style={styles.videoTimer}>
                        <Icon style={styles.iconTimer} name="md-stopwatch" /><Text style={{color: '#fff', fontSize: 12, paddingTop: 10}}>00:46:12</Text>
                    </View>
                    
                    <View style={styles.smallImage}>
                        <Image source={videoCallSmallImage} style={styles.videoCallSmallImage} />
                    </View>

                    <View style={styles.footerButtons}>
                        <View style={styles.roundedBtn}>
                            <Button transparent light>
                                <Icon color='black' style={styles.iconItem} name="ios-volume-low" />
                            </Button>
                        </View>
                        <View style={styles.roundedBtn}>
                            <Button transparent light>
                                <Icon color='black' style={styles.iconItem} name="md-mic" />
                            </Button>
                        </View>
                        <View style={{marginRight: 10}}>
                            <Button transparent light>
                                <Image source={videoCallDisconnect} style={styles.videoCallDisconnect} />
                            </Button>
                        </View>
                        <View style={styles.roundedBtn}>
                            <Button transparent light>
                                <Icon color='black' style={styles.iconItem} name="md-videocam" />
                            </Button>
                        </View>
                        <View style={styles.roundedBtn}>
                            <Button transparent light>
                                <Icon color='black' style={styles.iconItem} name="md-text" />
                            </Button>
                        </View>
                    </View>

                    <ScrollView horizontal style={styles.footerImages}>
                        <Image source={videoCallSmallImage} style={styles.footerImage} />
                        <Image source={VideoChat} style={styles.footerImage} />
                        <Image source={videoCallSmallImage} style={styles.footerImage} />
                        <Image source={VideoChat} style={styles.footerImage} />
                    </ScrollView>

			    </Content>
			</Container>
        );
    }
}

export default VideoCall;

