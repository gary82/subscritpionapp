import { Platform } from 'react-native';

const styles = {
	videoCallContainer: {
		flex: 1
	},
	videoCallImage: {
		height: 670,
		bottom: 0,
		flex: 1,
        resizeMode: "stretch",
        justifyContent: 'center'
	},
	smallImage: {
		borderRadius: 100,
		top: 40,
		right: 20,
		position: 'absolute'
	},
	videoCallSmallImage: {
		height: 70,
		width: 110
	},
	videoTimer: {
		top: 40,
		left: 20,
		position: 'absolute',
		flex: 1,
		flexDirection: 'row'
	},
	footerImages: {
		bottom: 10,
		padding: 20,
		position: 'absolute'
	},
	footerImage: {
		height: 70,
		width: 95,
		marginRight: 10
	},
	footerButtons: {
		flex: 1,
		flexDirection: 'row',
		position: 'absolute',
		bottom: 100,
		padding: 20,
		left: 30,
		alignItems: 'center'
	},
	iconItem: {
        height: 50,
        width: 50,
        left: -3
    },
    iconTimer: {
    	height: 25,
        width: 25,
        color: '#fff'
    },
    videoCallDisconnect: {
    	height: 60,
    	width: 60,
    	borderRadius: 30
    },
    roundedBtn: {
    	borderRadius: 50,
    	height: 45,
    	width: 45,
    	marginRight: 10,
    	paddingTop: 9,
    	paddingLeft: 0,
	    borderWidth: 1,
	    borderColor: '#fff'
    },
    // group video call screen styles
    groupVideoFooterButtons: {
    	flex: 1,
		flexDirection: 'row',
		position: 'absolute',
		bottom: 10,
		padding: 20,
		left: 30,
		alignItems: 'center'
    },
    groupPhotos: {
    	flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        justifyContent: 'space-around'
    },
    groupPhoto: {
    	width: '33%',
    	height: 135
    }
}

export default styles;
