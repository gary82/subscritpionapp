import { Platform } from 'react-native';

const styles = {

    header: {
        alignItems: 'center'
    },
	headerStyle: {
		backgroundColor: 'white',
        marginTop: 15,
        height: 45,
        paddingBottom: 15,
        color: '#3d4c4f',
        borderBottomWidth: 1,
        borderColor: '#f3f2f2'
	},
    userAvtar: {
        width: 30,
        height: 30,
        borderRadius: 10
    },
    headerIcon: {
        paddingTop: -10
    },
    iconItem: {
        height: 30,
        width: 30,
        color: '#000'
    },
    inputContainer: {
        width: 240,
        left: -20,
        borderBottomWidth: 0
    },
    inputField: {
        fontSize: 14,
        color: '#3d4c4f'
    },
    list: {
        padding: 15,
        backgroundColor: '#f3f2f2'
    },
    listHeader: {
        alignItems: 'center',
        margin: 5,
        marginBottom: 20,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    listHeaderText: {
        fontSize: 36,
        color: '#3d4c4f',
    },
    card: {
        flex: 1,
        marginLeft: 0,
        justifyContent: 'space-around'
    },
    cardButton: {
        flex: 1,
        marginBottom: 40
    },
    listBody: {
        marginLeft: 20,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    listNote: {
        width: 250,
        fontSize: 12,
        color: '#3d4c4f'
    },
    secondNote: {
        width:250,
        fontSize: 10,
        color: '#3d4c4f'
    },
    badge: {
        left: 50,
        top: 10,
        height: 25,
        width: 25,
        position: 'absolute'
    },
    badgeText: {
        fontSize: 12
    },

    // chat screen
    messageInputContainer: {
        width: 240,
        left: -90,
        borderBottomWidth: 0
    },
    messageInputField: {
        fontSize: 14,
        color: '#3d4c4f'
    },
    messageThreads: {
        alignItems: 'center',
        borderTopWidth: 1,
        borderColor: '#D3D3D3',
        paddingTop: 10,
        borderRadius: 20
    },
    messageCard: {
        width: 350,
        borderWidth: 0
    },
    messageInnerView: {
        flex: 1,
        flexDirection: 'column',
        marginBottom: -15,
    },
    messageBody: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginLeft: 20
    },
    messageBodyWithImage: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    messageBodyYou: {
        backgroundColor: '#d0eff4',
        padding: 10,
        marginTop: 10,
        marginLeft: 10
    },
    messageBodyAnother: {
        backgroundColor: '#60B2C3',
        padding: 10,
        marginRight: 10,
        marginTop: 10
    },
    messageTextYou: {
        color: '#3d4c4f',
        fontSize: 10
    },
    messageTime: {
        fontSize: 8,
        marginTop: 5,
        marginBottom: 15,
        color: '#A9A9A9'
    },
    messageTextAnother: {
        color: '#fff',
        fontSize: 10
    },
    // join a group css
    joinAGroupContainer: {
        paddingTop: 5,
        backgroundColor: '#f3f2f2'
    },
    groupCards: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        justifyContent: 'space-around'
    },
    groupCard: {
        width: '45%',
    },
    groupImage: {
        width: 100,
        height: 100
    },
    groupTitle: {
        fontSize: 14,
        color: '#3d4c4f',
        marginBottom: 10
    },
    groupPrice: {
        fontSize: 14,
        color: '#3d4c4f',
        marginBottom: 10,
    },
    groupTime: {
        fontSize: 8,
        color: '#3d4c4f',
        marginBottom: 10
    },
    groupIcon: {
        position: 'absolute',
        right: 0,
        top: -15
    },
    groupCardItem: {
        flex: 1,
        flexDirection: 'column',
        paddingTop: 15,
        paddingBottom: 20
    },
    joinGroupButton: {
        backgroundColor: '#3fb4c6',
        height: 30,
        padding: 10
    }
}

export default styles;
