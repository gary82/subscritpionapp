import React, { Component } from 'react';

import { Image } from 'react-native';

import {
	Container,
	Content,
	Text,
    Left,
    Right,
    Thumbnail,
    Button,
    Input,
    Item,
    View,
    Footer,
    Icon,
    Body,
    Card,
    CardItem
} from 'native-base';

import _ from 'lodash';

import styles from './styles';
import avatar from '../../assets/images/avatar.png';

const CustomHeader = ({ title, subtitle }) => (
    <View style={styles.header}>
        <Text style={{fontSize: 14}}>{title}</Text>
        <Text style={{fontSize: 10}}>{subtitle}</Text>
    </View>
);

const chatMessages = {
    YESTERDAY: [
        {
            sender: 'you',
            avtar: avatar,
            time: 'Yesterday 14:46pm',
            messages: [
                "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
            ]
        },
        {
            sender: 'another',
            avtar: avatar,
            time: 'Yesterday 14:46pm',
            messages: [
                "Various versions have evolved over the evolved over",
                "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
            ]
        }
    ],
    TODAY: [
        {
            sender: "you",
            avtar: avatar,
            time: '2h ago',
            messages: [
                "Various versions have evolved over the years,sometimes by accident, so",
                "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
            ]
        },
        {
            sender: "another",
            avtar: avatar,
            time: 'less than 1 min ago',
            messages: [
                "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
            ]
        }
    ]
}

const MessagePreview = function(props) {

    const value = props.props

    return(
        <Card transparent style={styles.messageCard}>
            <CardItem>
                <Left>
                    {
                        value.sender === 'you'
                    &&
                        <View style={styles.messageInnerView}>
                            {
                                _.map(value.messages, (message, key) => {
                                    return(
                                        <View style={styles.messageBody}>
                                            {
                                                (value.messages.length - 1) === key
                                            ?
                                                <View style={styles.messageBodyWithImage}>
                                                    <Image style={{height: 40, width: 40, bottom: -20, marginLeft: -30}} source={value.avtar} />
                                                    <View style={{left: 19, paddingRight: 14}}>
                                                        <Body style={styles.messageBodyYou}><Text style={styles.messageTextYou}>{message}</Text></Body>
                                                    </View>
                                                </View>
                                            :
                                                <View style={{left: 28, paddingRight: 18}}>
                                                    <Body style={styles.messageBodyYou}><Text style={styles.messageTextYou}>{message}</Text></Body>
                                                </View>
                                            }
                                        </View>
                                    )
                                })
                            }
                            <Text style={{...styles.messageTime, left: 60}}>{value.time}</Text>
                        </View>
                    }
                    
                    {
                        value.sender === 'another'
                    &&
                        <View style={{...styles.messageInnerView, paddingRight: 18}}>
                            {
                                _.map(value.messages, (message, key) => {
                                    return(
                                        <View style={styles.messageBody}>
                                            {
                                                (value.messages.length - 1) === key
                                            ?
                                                <View style={styles.messageBodyWithImage}>
                                                    <View style={{left: 13, paddingRight: 25}}>
                                                        <Body style={styles.messageBodyAnother}><Text style={styles.messageTextAnother}>{message}</Text></Body>
                                                    </View>
                                                    <Image style={{height: 40, width: 40, bottom: -20}} source={value.avtar} />
                                                </View>
                                            :
                                                <View style={{paddingLeft: 17, paddingRight: 27}}>
                                                    <Body style={{...styles.messageBodyAnother, marginLeft: 0}}><Text style={styles.messageTextAnother}>{message}</Text></Body>
                                                </View>
                                            }
                                        </View>
                                    )
                                })
                            }
                            <Text style={{...styles.messageTime, left: 40}}>{value.time}</Text>
                        </View>
                    }
                </Left>
            </CardItem>
        </Card>
    )
}

class GroupChat extends Component {

	static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: <CustomHeader title={navigation.state.params.title} subtitle={navigation.state.params.date} />,
            headerRight: <Thumbnail style={{...styles.userAvtar, marginRight: 10}} source={ navigation.state.params.avatar } />,
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            message: ''
        };
    }

    componentWillMount() {
    }

    onChange(name, value) {
        this.setState({
            [name]: value
        });
    }

    render() {
        const { message } = this.state;
        const chats = [1,2,3,4,5,6,7,8,9]
        return (
            <Container>
            	<Content>
                    {
                        _.map(chatMessages, (message, key) => {
                            return(
                                <View style={styles.messageThreads} key={key}>
                                    <Text style={{fontSize: 12, color: '#A9A9A9'}}>{key}</Text>
                                    {
                                        _.map(message, (value, index) => {
                                            return(
                                                <MessagePreview props={value} key={index} />
                                            )
                                        })
                                    }
                                </View>
                            )
                        })
                    }
			    </Content>
                <Footer>
                    <Left>
                        <Button dark transparent>
                            <Icon style={styles.iconItem} name="md-attach" />
                        </Button>
                    </Left>
                    <Body>
                        <Item style={styles.messageInputContainer}>
                            <Input
                                placeholder='Type Your Message'
                                placeholderTextColor='#3d4c4f'
                                style={styles.messageInputField}
                                value={message}
                                name='message'
                                onChangeText={(value) => this.onChange('message', value)} />
                        </Item>
                    </Body>
                    <Right>
                        <Button dark transparent>
                            <Icon color='black' style={styles.iconItem} name="md-arrow-round-forward" />
                        </Button>
                    </Right>
                </Footer>
			</Container>
        );
    }
}

export default GroupChat;

