import React, { Component } from 'react';

import {
    Image
} from 'react-native';

import {
	Container,
	Content,
	Text,
    Left,
    Right,
    Button,
    View,
    Icon,
    Body,
    Card,
    CardItem
} from 'native-base';

import _ from 'lodash';

import styles from './styles';
import avatar from '../../assets/images/avatar.png';

const GroupCard = function(props) {
    return(
        <Card transparent style={styles.groupCard}>
            <CardItem style={styles.groupCardItem}>
                <Body style={{alignItems: 'center', marginBottom: 10}}>
                    <Image style={styles.groupImage} source={avatar} />
                    <Icon style={styles.groupIcon} name="md-more" />
                </Body>
                <View style={{alignItems: 'center', borderBottomWidth: 1, borderColor: '#f3f3f3'}}>
                    <Text style={styles.groupTitle}>
                        My Pilgrimage
                    </Text>
                    <Text style={styles.groupTime}>
                        Tuesday 09:00 pm - 10:00 pm
                    </Text>
                </View>
                
                <View style={{alignItems: 'center'}}>
                    <Text style={styles.groupPrice}>
                        $39/month
                    </Text>
                </View>

                <View style={{alignItems: 'center'}}>
                    <Button rounded style={styles.joinGroupButton}>
                        <Text style={{fontSize: 12}}>Join Group</Text>
                    </Button>
                </View>
           
            </CardItem>
        </Card>
    )
}

class JoinGroup extends Component {

	static navigationOptions = ({ navigation }) => {
        return {
            header: null
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            groupId: ''
        };
    }

    componentWillMount() {
    }

    onChange(name, value) {
        this.setState({
            [name]: value
        });
    }

    render() {
        const { groupId } = this.state;

        return (
            <Container>
            	<Content padder style={styles.joinAGroupContainer}>
                    <View style={{alignItems: 'center', margin: 10}}>
                        <Text style={{...styles.listHeaderText, fontWeight: 'normal'}}>
                            Join A Group
                        </Text>
                    </View>
                    <View style={styles.groupCards}>
                        <GroupCard />
                        <GroupCard />
                        <GroupCard />
                        <GroupCard />
                        <GroupCard />
                        <GroupCard />
                        <GroupCard />
                        <GroupCard />
                    </View>
			    </Content>
			</Container>
        );
    }
}

export default JoinGroup;

