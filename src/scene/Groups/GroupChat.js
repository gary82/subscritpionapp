import React, { Component } from 'react';

import { Image } from 'react-native';

import {
	Container,
	Content,
	Text,
    Left,
    Right,
    Thumbnail,
    Button,
    Input,
    Item,
    View,
    Footer,
    Icon,
    Body,
    Card,
    CardItem
} from 'native-base';

import _ from 'lodash';

import styles from './styles';
import avatar from '../../assets/images/avatar.png';

const CustomHeader = ({ title, subtitle }) => (
    <View style={styles.header}>
        <Text style={{fontSize: 14}}>{title}</Text>
        <Text style={{fontSize: 10}}>{subtitle}</Text>
    </View>
);

const chatMessages = {
    YESTERDAY: [
        {
            sender: 'you',
            avtar: avatar,
            time: 'Yesterday 14:46pm',
            messages: [
                "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humou",
                "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like humour and the likenhum",
            ]
        },
        {
            sender: 'another',
            avtar: avatar,
            time: 'Yesterday 14:46pm',
            messages: [
                "Various versions have evolved (injected humour and the like). the year",
                "Various versions have evolved (injected humour and the like). evolved over the years, sometimes by accident, sometimes on purpose (inject",
            ]
        }
    ],
    TODAY: [
        {
            sender: "you",
            avtar: avatar,
            time: '2h ago',
            messages: [
                "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humou",
                "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). evolved over the years, sometimes by accident, sometimes on purpose"
            ]
        },
        {
            sender: "another",
            avtar: avatar,
            time: 'less than 1 min ago',
            messages: [
                "Various versions have evolved ovemetimes on purpose (injected humour and the like).he years, sometim",
            ]
        }
    ]
}

const MessagePreview = function(props) {

    const value = props.props

    return(
        <View transparent style={styles.messageCard}>

            {
                value.sender === 'you'
            &&
                <View style={styles.messageInnerView}>
                    {
                        _.map(value.messages, (message, key) => {
                            return(
                                <View>
                                    {
                                        (value.messages.length - 1) === key
                                    ?
                                        <View style={styles.messageBodyWithImage}>
                                            <Image style={{height: 40, width: 40, bottom: -20}} source={value.avtar} />
                                            <View style={{...styles.messageBodyYou, paddingLeft: '1%'}}>
                                                <Body><Text style={styles.messageTextYou}>{message}</Text></Body>
                                            </View>
                                        </View>
                                    :
                                        <View style={styles.messageBodyWithImage}>
                                            <View style={{...styles.messageBodyYou, paddingLeft: '21%'}}>
                                                <Body><Text style={styles.messageTextYou}>{message}</Text></Body>
                                            </View>
                                        </View>
                                    }
                                </View>
                            )
                        })
                    }
                    <Text style={{...styles.messageTime, paddingLeft: '21%'}}>{value.time}</Text>
                </View>
            }
            
            {
                value.sender === 'another'
            &&
                <View style={styles.messageInnerView}>
                    {
                        _.map(value.messages, (message, key) => {
                            return(
                                <View>
                                    {
                                        (value.messages.length - 1) === key
                                    ?
                                        <View style={styles.messageBodyWithImage}>
                                            <View style={styles.messageBodyAnother}>
                                                <Body><Text style={styles.messageTextAnother}>{message}</Text></Body>
                                            </View>
                                            <Image style={{height: 40, width: 40, bottom: -20}} source={value.avtar} />
                                        </View>
                                    :
                                        <View style={{...styles.messageBodyWithImage, right: 0}}>
                                            <View style={styles.messageBodyAnother}>
                                                <Body><Text style={styles.messageTextAnother}>{message}</Text></Body>
                                            </View>
                                        </View>
                                    }
                                </View>
                            )
                        })
                    }
                    <Text style={{...styles.messageTime, paddingLeft: '21%'}}>{value.time}</Text>
                </View>
            }

        </View>
    )
}

class GroupChat extends Component {

	static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: <CustomHeader title={navigation.state.params.title} subtitle={navigation.state.params.date} />,
            headerRight: <Thumbnail style={{...styles.userAvtar, marginRight: 10}} source={ navigation.state.params.avatar } />,
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            message: ''
        };
    }

    componentWillMount() {
    }

    onChange(name, value) {
        this.setState({
            [name]: value
        });
    }

    render() {
        const { message } = this.state;
        const chats = [1,2,3,4,5,6,7,8,9]
        return (
            <Container>
            	<Content style={{paddingLeft: 10, paddingRight: 15}}>
                    
                    {
                        _.map(chatMessages, (message, key) => {
                            return(
                                <View style={styles.messageThreads} key={key}>
                                    <Text style={{fontSize: 12, color: '#A9A9A9'}}>{key}</Text>
                                    {
                                        _.map(message, (value, index) => {
                                            return(
                                                <MessagePreview props={value} key={index} />
                                            )
                                        })
                                    }
                                </View>
                            )
                        })
                    }
                    
			    </Content>
                <Footer>
                    <Left>
                        <Button dark transparent>
                            <Icon style={styles.iconItem} name="md-attach" />
                        </Button>
                    </Left>
                    <Body>
                        <Item style={styles.messageInputContainer}>
                            <Input
                                placeholder='Type Your Message'
                                placeholderTextColor='#3d4c4f'
                                style={styles.messageInputField}
                                value={message}
                                name='message'
                                onChangeText={(value) => this.onChange('message', value)} />
                        </Item>
                    </Body>
                    <Right>
                        <Button dark transparent>
                            <Icon color='black' style={styles.iconItem} name="md-arrow-round-forward" />
                        </Button>
                    </Right>
                </Footer>
			</Container>
        );
    }
}

export default GroupChat;

