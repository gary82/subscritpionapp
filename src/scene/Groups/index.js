import React, { Component } from 'react';

import {
	Container,
	Header,
	Content,
	Text,
    List,
    ListItem,
    Left,
    Body,
    Right,
    Thumbnail,
    Button,
    Input,
    Item,
    Card,
    CardItem,
    View,
    Icon,
    Badge
} from 'native-base';

import _ from 'lodash';

import styles from './styles';
import avatar from '../../assets/images/avatar.png';

const ListItemComponent = function(props){

    return(
        <Button transparent onPress={ () => props.goToChatScreen(props.groupName, props.groupDate, props.avatar) } style={styles.cardButton}>
            <Card style={styles.card}>
                <CardItem>
                    <Thumbnail source={ props.avatar } />
                    {
                        props.badge
                    &&
                        <Badge info style={styles.badge}>
                            <Text style={styles.badgeText}>{props.badge}</Text>
                        </Badge>
                    }
                        
                    <Body style={styles.listBody}>
                        <Text>{props.groupName}</Text>
                        <Text note style={styles.secondNote}>{props.groupDate}</Text>
                        <Text note style={styles.listNote}>{props.groupDes}</Text>
                    </Body>
                    
                    <Right style={{right: -30, top: -20}}>
                        <Icon style={styles.iconItem} name="md-more" />
                    </Right>

                </CardItem>
            </Card>
        </Button>
    )
}

class Groups extends Component {

	static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            searchItem: ''
        };
    }

    componentWillMount() {
    }

    onChange(name, value) {
        this.setState({
            [name]: value
        });
    }

    goToChatScreen = (name, date, avatar) => {
        this.props.navigation.navigate('GroupChat', {title: name, date: date, avatar: avatar});
    }

    render() {
        const { searchItem } = this.state;
        const chats = [1,2,3,4,5,6,7,8,9];

        return (
            <Container>
            	<Header style={styles.headerStyle}>
                    <Left style={styles.headerIcon}>
                        <Icon style={styles.iconItem} name="md-search" />
                    </Left>
                    <Body>
                        <Item style={styles.inputContainer}>
                            <Input
                                placeholder='What are looking for?'
                                placeholderTextColor='#3d4c4f'
                                style={styles.inputField}
                                value={searchItem}
                                name='searchItem'
                                onChangeText={(value) => this.onChange('searchItem', value)} />
                        </Item>
                    </Body>
                    <Right>
                        <Thumbnail style={styles.userAvtar} source={ avatar } />
                    </Right>
            	</Header>
            	<Content>
			    	<List style={styles.list}>
                        <View style={styles.listHeader}>
                            <Text style={styles.listHeaderText}>Chats</Text>
                            <Button onPress={ () => this.props.navigation.navigate('JoinGroup') } transparent><Text>Join</Text></Button>
                            <Button onPress={ () => this.props.navigation.navigate('VideoCall') } transparent><Text>video1</Text></Button>
                            <Button onPress={ () => this.props.navigation.navigate('GroupVideoCall') } transparent><Text>group video</Text></Button>
                        </View>
                        {
                            _.map(chats, ( chat, key ) => {
                                return(
                                    <ListItemComponent
                                        goToChatScreen={this.goToChatScreen}
                                        avatar={avatar}
                                        key={key}
                                        badge={3}
                                        groupName='My Pilgrimage'
                                        groupDate='Neil Harbison - Yesterday'
                                        groupDes='Evil true high lady roof men had open...' />
                                )
                            })
                        }

                    </List>
			    </Content>
			</Container>
        );
    }
}

export default Groups;

