import { Platform } from 'react-native';

const styles = {
    mainContainer: {
      	flex: 1,
      	flexDirection: 'column',
      	backgroundColor: '#ffffff',
        justifyContent: 'space-between',
        paddingTop: 100
    },
    innerContent: {
      	flex: 1,
      	flexDirection: 'column'
    },
    appLogoView: {
        alignItems: 'center'
   	},
   	appLogo: {
      	height: 85,
      	width: 95
    },
   	loginFormUpper: {
     		flex: 1,
     		paddingTop: 30
   	},
   	signInText: {
        alignItems: 'center'
   	},
   	form: {
        marginTop: 30,
   	},
   	inputContainer: {
     		marginLeft: 15,
     		marginRight: 15,
     		marginBottom: 20,
     		backgroundColor: '#3d4c4f',
     		height: 62,
     		borderRadius: 30
   	},
   	inputField: {
        color: '#ffffff',
        paddingLeft: 15,
        textAlign: 'center'
   	},
   	facebookLogin: {
     		marginLeft: 70,
      	alignItems: 'center',
        marginTop: 10
   	},
   	directionImage: {
     		left: 80,
     		margin: 10
   	},
    directionR: {
      	height: 30,
      	width: 30,
        color: '#fff'
    },
    saveButton: {
      margin: 10,
      left: '38%',
      alignItems: 'center'
    },
    saveImage: {
      height: 70,
      width: 70,
      borderRadius: 30
    },
    fbIcon: {
      height: 20,
      width: 20
    }
}

export default styles;
