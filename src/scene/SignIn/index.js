import React, { Component } from 'react';

import { Image } from 'react-native';
import {
	Container,
	Content,
    Icon,
	Button,
	Text,
    Input,
    Form,
    Item,
    Footer,
    View
} from 'native-base';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import styles from './styles';

import signInUpButton from '../../assets/images/signInUpButton.jpg';
import appLogo from '../../assets/images/logo11.png';
import fbIcon from '../../assets/images/fbiCon-128.png';

class SignIn extends Component {

    static navigationOptions = {
        title: '',
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    componentWillMount() {
    }

    onChange(name, value) {
        this.setState({
            [name]: value
        });
    }

    signIn() {
        this.props.navigation.navigate("Groups")
    }

    render() {

        const { username, password } = this.state;

        return (
        	<View style={styles.mainContainer}>
                <View style={styles.innerContent}>
                    <View style={styles.appLogoView}>
                        <Image style={styles.appLogo} source={appLogo}/>
                    </View>

                    <View style={styles.loginFormUpper}>
                        <View style={styles.loginForm}>
                            <View style={styles.signInText}>
                                <Text style={{fontSize: 26, color: '#3d4c4f'}}>Sign In</Text>
                            </View>
                            <Form style={styles.form}>
                                <Item style={styles.inputContainer}>
                                    <Input
                                        style={styles.inputField}
                                        placeholder='Username'
                                        value={username}
                                        placeholderTextColor='#ffffff'
                                        name='username'
                                        onChangeText={(value) => this.onChange('username', value)} />
                                </Item>
                                <Item style={styles.inputContainer}>
                                    <Input
                                        style={styles.inputField}
                                        secureTextEntry
                                        type="password"
                                        placeholder='Password'
                                        value={password}
                                        placeholderTextColor='#ffffff'
                                        name='password'
                                        onChangeText={(value) => this.onChange('password', value)}  />
                                </Item>
                                <View style={styles.saveButton}>
                                    <Button transparent onPress={ () => this.signIn() }>
                                        <Image style={styles.saveImage} source={signInUpButton} />
                                    </Button>
                                </View>
                            </Form>
                        </View>

                        <View style={styles.facebookLogin}>
                            <Button
                                dark
                                transparent>
                                <Image source={fbIcon} style={styles.fbIcon} /><Text style={{fontSize: 20, color: '#3d4d4d'}}>Sign In With Facebook</Text>
                            </Button>
                        </View>
                    </View>
                </View>

                <Footer style={{backgroundColor: '#3fb4c6'}}>
                    <Button
                        transparent
                        light
                        onPress={ () => this.props.navigation.navigate("SignUp") }>
                        <Text style={{fontSize: 22, left: 20}}>Create An Account</Text>
                        <View style={styles.directionImage}><Icon name="ios-arrow-forward" style={styles.directionR} /></View>
                    </Button>
                    
                </Footer>

			</View>
    
        );
    }
}

export default SignIn;

