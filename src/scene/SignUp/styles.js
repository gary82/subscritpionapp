import { Platform } from 'react-native';

const styles = {

	headerStyle: {
		backgroundColor: 'white',
	    height: 65,
        color: '#3d4c4f',
	    borderBottomWidth: 0
	},
	headerTitleStyle: {
		fontSize: 28,
		color: '#3d4c4f',
		fontWeight: 'normal'
	},
    mainContainer: {
        flex: 1
    },
    inputContainer: {
    	height: 70,
    	marginTop: 4,
    	paddingTop: 20
    },
    inputField: {
    	fontSize: 20,
        textAlign: 'center'
    },
    saveButton: {
    	margin: 60,
    	left: '20%',
    	alignItems: 'center'
    },
    saveImage: {
    	height: 100,
    	width: 100,
    	borderRadius: 50
    }
}

export default styles;
