import React, { Component } from 'react';

import { View, Image } from 'react-native';
import {
	Container,
	Header,
	Content,
	Button,
	Text,
	Form,
	Input,
	Item
} from 'native-base';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import styles from './styles';

import signInUpButton from '../../assets/images/signInUpButton.jpg';

class SignUp extends Component {

	static navigationOptions = ({ navigation }) => {
        const { state: { params } } = navigation;

        return ({
            headerTitle: 'Create Account',
            headerStyle: styles.headerStyle,
            headerTitleStyle: styles.headerTitleStyle
        });

    };

	constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            repeatPassword: ''
        };
    }

    componentWillMount() {
    }

    onChange(name, value) {
        this.setState({
            [name]: value
        });
    }

    signUp() {
        this.props.navigation.navigate("Groups")
    }

    render() {

    	const { firstName, lastName, email, password, repeatPassword } = this.state;

        return (
            <Container style={styles.mainContainer}>
            	<Content>
			    	<Form>
                        <Item style={styles.inputContainer}>
                            <Input
                                placeholder='First Name'
                                placeholderTextColor='#3d4c4f'
                                style={styles.inputField}
                                value={firstName}
                                name='firstName'
                                onChangeText={(value) => this.onChange('firstName', value)} />
                        </Item>
                        <Item style={styles.inputContainer}>
                            <Input
                                placeholder='Last Name'
                                placeholderTextColor='#3d4c4f'
                                style={styles.inputField}
                                value={lastName}
                                name='lastName'
                                onChangeText={(value) => this.onChange('lastName', value)} />
                        </Item>
                        <Item style={styles.inputContainer}>
                            <Input
                                placeholder='Email Address'
                                placeholderTextColor='#3d4c4f'
                                style={styles.inputField}
                                value={email}
                                name='email'
                                onChangeText={(value) => this.onChange('email', value)} />
                        </Item>
                        <Item style={styles.inputContainer}>
                            <Input
                                secureTextEntry
                                placeholder='Password'
                                placeholderTextColor='#3d4c4f'
                                style={styles.inputField}
                                value={password}
                                name='password'
                                onChangeText={(value) => this.onChange('password', value)}  />
                        </Item>
                        <Item style={styles.inputContainer}>
                            <Input
                                secureTextEntry
                                placeholder='Repeat Password'
                                placeholderTextColor='#3d4c4f'
                                style={styles.inputField}
                                value={repeatPassword}
                                name='repeatPassword'
                                onChangeText={(value) => this.onChange('repeatPassword', value)}  />
                        </Item>

                        <View style={styles.saveButton}>
                            <Button transparent onPress={ () => this.signUp() }>
                                <Image style={styles.saveImage} source={signInUpButton} />
                            </Button>
                        </View>
                    </Form>
			    </Content>
			</Container>
        );
    }
}

export default SignUp;

